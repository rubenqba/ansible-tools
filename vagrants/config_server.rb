Vagrant.configure("2") do |config|

    config.vm.define :deploy do |carrier|
        carrier.vm.provider "virtualbox" do |v|
            v.memory = 1024
            v.cpus = 2
        end
        carrier.vm.provision "ansible" do |ansible|
            ansible.compatibility_mode = "2.0"
            ansible.galaxy_roles_path = "./roles"
            ansible.playbook = "playbooks/deploy-configserver.yml"
            ansible.host_vars = {
                "default" => {
                    ansible_python_interpreter: "/usr/bin/python3",
                }
            }    
            ansible.extra_vars = {
                app_domain: "docker.local",
                app_version: "latest",
                gitlab_user: "rbresler@binbit.net",
                gitlab_token: "sgGxSBoCg1xdJWUyjF8r",
                users_allowed: [ 'vagrant', 'rbresler' ],
                lb_type: "traefik"
            }
        end
        carrier.vm.network "forwarded_port", guest: 8080, host: 18080
        carrier.vm.network "forwarded_port", guest: 80, host: 80
        carrier.vm.network "forwarded_port", guest: 443, host: 443     
    end    
end
