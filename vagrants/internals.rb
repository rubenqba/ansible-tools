Vagrant.configure("2") do |config|
    config.vm.box = "ubuntu/bionic64"
    config.vm.provider "virtualbox" do |v|
        v.memory = 3196
        v.cpus = 2
    end
    config.vm.provision "ansible" do |ansible|
        ansible.playbook = "internals.yml"
        # ansible.become = true
        ansible.host_vars = {
            "default" => {
                ansible_python_interpreter: "/usr/bin/python3",
            }
        }        
        ansible.extra_vars = {
            lb_version: "v1.7",
            domain_name: "docker.local",
            gitlab_user: "rbresler@binbit.net",
            gitlab_token: "sgGxSBoCg1xdJWUyjF8r",
            nexus_admin: "rubenqba",
            nexus_key: "ciberbsbneo",
            users_allowed: [ 'vagrant', 'rbresler' ]
        }
    end
    config.vm.network "forwarded_port", guest: 8080, host: 18080
    config.vm.network "forwarded_port", guest: 80, host: 80
    config.vm.network "forwarded_port", guest: 443, host: 443    
end