# -*- mode: ruby -*-
# vi: set ft=ruby :
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
    config.vm.box = "ubuntu/bionic64"
    config.vm.provider "virtualbox" do |v|
        v.memory = 3196
        v.cpus = 2
    end
    config.vm.provision "ansible" do |ansible|
        ansible.playbook = "internals.yml"
        # ansible.become = true
        ansible.host_vars = {
            "default" => {
                ansible_python_interpreter: "/usr/bin/python3",
            }
        }        
        ansible.extra_vars = {
            lb_version: "v1.7",
            domain_name: "docker.local",
            gitlab_user: "rbresler@binbit.net",
            gitlab_token: "sgGxSBoCg1xdJWUyjF8r",
            nexus_admin: "rubenqba",
            nexus_key: "ciberbsbneo",
            users_allowed: [ 'vagrant', 'rbresler' ]
        }
    end
    config.vm.network "forwarded_port", guest: 8080, host: 18080
    config.vm.network "forwarded_port", guest: 80, host: 80
    config.vm.network "forwarded_port", guest: 443, host: 443    
    # config.vm.provision "ansible" do |ansible|
    #     ansible.playbook = "traefik.yml"
    #     ansible.become = true
    #     ansible.host_vars = {
    #         "default" => {
    #             ansible_python_interpreter: "/usr/bin/python3",
    #         }
    #     }        
    #     ansible.extra_vars = {
    #         app_version: "v1.7",
    #         users_allowed: [ 'vagrant', 'rbresler' ]
    #     }
    # end
    # config.vm.network "forwarded_port", guest: 8080, host: 9080
    # config.vm.network "forwarded_port", guest: 80, host: 8080
    # config.vm.network "forwarded_port", guest: 443, host: 8443    
    # config.vm.provision "ansible" do |ansible|
    #     ansible.playbook = "pgbouncer.yml"
    #     ansible.become = true
    # end
    config.vm.provision "ansible" do |ansible|
        ansible.playbook = "deploy-carrier.yml"
        ansible.become = true
        ansible.extra_vars = {
            config_version: "latest",
            app_name: "ph-smart",
            app_version: "latest",
            users_allowed: [ 'vagrant', 'rbresler' ],
            app_allow_billing: true,
            app_allow_optin: true
        }
    end
    config.vm.network "forwarded_port", guest: 8080, host: 18080
    config.vm.network "forwarded_port", guest: 80, host: 80
    config.vm.network "forwarded_port", guest: 443, host: 443     
    # config.vm.provision "ansible" do |ansible|
    #     ansible.playbook = "python-test.yml"
    #     ansible.become = true
    # end
    # config.vm.network "forwarded_port", guest: 80, host: 8080
    # config.vm.provision "ansible" do |ansible|
    #     ansible.playbook = "deploy-carrier.yml"
    #     ansible.become = true
    #     ansible.host_vars = {
    #         "default" => {
    #             ansible_python_interpreter: "/usr/bin/python3",
    #         }
    #     }        
    #     ansible.extra_vars = {
    #         config_version: "latest",
    #         app_name: "lk-kitmaker",
    #         app_version: "latest",
    #         users_allowed: [ 'vagrant', 'rbresler' ]
    #     }
    # end
    # config.vm.network "forwarded_port", guest: 8080, host: 9080
    # config.vm.network "forwarded_port", guest: 80, host: 8080
    # config.vm.network "forwarded_port", guest: 443, host: 8443
    # config.vm.provision "ansible" do |ansible|
    #     ansible.playbook = "deploy-configserver.yml"
    #     ansible.become = true
    #     ansible.host_vars = {
    #         "default" => {
    #             ansible_python_interpreter: "/usr/bin/python3",
    #         }
    #     }
    #     ansible.extra_vars = {
    #         users_allowed: [ 'vagrant', 'rbresler' ]
    #     }
    # end
    # config.vm.network "forwarded_port", guest: 443, host: 8443
    # config.vm.provision "ansible" do |ansible|
    #     ansible.playbook = "portainer.yml"
    #     ansible.become = true
    #     ansible.extra_vars = {
    #         users_allowed: [ 'vagrant', 'rbresler' ]
    #     }
    # end

    # configuracion para probar el gitlab-runner
    # config.vm.provision "ansible" do |ansible|
    #     ansible.playbook = "shell-runner.yml"
    #     ansible.become = true
    #     ansible.extra_vars = {
    #         gitlab_url: "https://gitlab.com/",
    #         gitlab_token: "fVDLiyfebjfp49iq3Hs2",
    #         gitlab_description: "vagrant test runner",
    #         gitlab_tags: "shell,docker"
    #     }
    # end

    # configuracion para nexus
    # config.vm.provision "ansible" do |ansible|
    #     ansible.playbook = "nexus.yml"
    #     ansible.become = true
    # end

    # configuracion para swap
    # config.vm.provision "ansible" do |ansible|
    #     ansible.playbook = "swap.yml"
    #     ansible.become = true
    # end

    # configuracion para docker
    # config.vm.provision "ansible" do |ansible|
    #     ansible.playbook = "deploy-docker.yml"
    #     ansible.become = true
    # end
end
