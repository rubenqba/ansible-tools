
docker run --rm -t -i -v $PWD/config:/etc/gitlab-runner \
    --name gitlab-runner gitlab/gitlab-runner \
    register --non-interactive \
    --url "http://gitlab.binbit.com/" \
    --registration-token "PQnM7JXJydN5LxACp-kj" \
    --executor "docker" \
    --docker-image "docker:stable" \
    --description "docker-runner" \
    --tag-list "docker" \
    --docker-privileged \
    --run-untagged \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock"
    --locked="false"