rubenqba.virtualbox
=========

Install VirtualBox.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: rubenqba.virtualbox, vbox_enable_extpack: true/false }

